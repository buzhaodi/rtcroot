import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import chat from '@/components/chat'
import onetoone from '@/components/onetoone'
import skonetoone from '@/components/skonetoone'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/chat',
      name: 'chat',
      component: chat
    },
    
    {
      path: '/onetoone',
      name: 'onetoone',
      component: onetoone
    },
    {
      path: '/skonetoone',
      name: 'skonetoone',
      component: skonetoone
    },
    
  ]
})
